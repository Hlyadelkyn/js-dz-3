// функции опять же способствуют сокращению и оптимизации кода, так как указавши раз функции нам не придётся много раз повторять одно и то же действие

// передавать аргументы функциям стоит в тех случаях когда у нас указано точное колвичество параметров, для некоторых функций неважно количество переданных ей аргументов но так же некоторые функции просто не смогут работать без четко переданных аргументов

let userNum1 = +prompt("enter first num");
let userNum2 = +prompt("enter secnd num");

while(userNum1=="" || isNaN(userNum1) || userNum2=="" || isNaN(userNum2)){ //проверка на корректность ввода
    userNum1 = +prompt("Error, Pls enter first num correctly");
    userNum2 = +prompt("Error, Pls enter secnd num correctly");
}

let operation = prompt("Enter method you need")

let opResult;

function getResult(a, b, c){
    switch(c){
        case "*":
            opResult = a * b;
            break;
        case "/":
            opResult = a / b;
            break;
        case "+":
            opResult = a + b;
            break;
        case "-":
            opResult = a - b;
            break;
        case "**":
            opResult = a ** b;
            break;
    }
    return `result of operation is ${opResult}`;
}

console.log(getResult(userNum1, userNum2, operation));
